package pl.softwareskill.course.spring.wizytownik.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.softwareskill.course.spring.wizytownik.model.Person;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface PersonRepository extends JpaRepository<Person, Long> {

    Optional<Person> findByPersonUuid(final UUID personUuid);

    Optional<Person> findByEmail(final String email);
}
