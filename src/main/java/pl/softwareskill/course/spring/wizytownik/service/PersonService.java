package pl.softwareskill.course.spring.wizytownik.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pl.softwareskill.course.spring.wizytownik.exception.PersonAlreadyExistsException;
import pl.softwareskill.course.spring.wizytownik.exception.PersonNotFoundException;
import pl.softwareskill.course.spring.wizytownik.model.Person;
import pl.softwareskill.course.spring.wizytownik.repository.PersonRepository;

import java.util.List;
import java.util.UUID;

@Service
@AllArgsConstructor
public class PersonService {

    private final PersonRepository personRepository;

    public Person getPerson(final UUID personUuid) {
        return personRepository.findByPersonUuid(personUuid).orElseThrow(PersonNotFoundException::new);
    }

    public List<Person> getPersons() {
        return personRepository.findAll();
    }

    public void save(Person person) {
        if(personRepository.findByEmail(person.getEmail()).isPresent()) {
            throw new PersonAlreadyExistsException();
        }
        person.setPersonUuid(UUID.randomUUID());
        person.getAddress().setAddressUuid(UUID.randomUUID());
        personRepository.save(person);
    }

    public void editPerson(final UUID personUuid, final Person person) {

        final Person personFromDb = getPerson(personUuid);

        personFromDb.setEmail(person.getEmail());
        personFromDb.setName(person.getName());
        personFromDb.setSurname(person.getSurname());
        personFromDb.setTelephone(person.getTelephone());

        personFromDb.getAddress().setCity(person.getAddress().getCity());
        personFromDb.getAddress().setStreet(person.getAddress().getStreet());
        personFromDb.getAddress().setStreetNumber(person.getAddress().getStreetNumber());
        personFromDb.getAddress().setHomeNumber(person.getAddress().getHomeNumber());

        personRepository.save(personFromDb);
    }

    public void delete(final UUID personUuid) {
        checkIsPersonExists(personUuid);
        final Person person = getPerson(personUuid);
        personRepository.delete(person);
    }

    private void checkIsPersonExists(UUID personUuid) {
        if(!personRepository.findByPersonUuid(personUuid).isPresent()) {
            throw new PersonNotFoundException();
        }
    }
}
